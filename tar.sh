#!/bin/bash

# demander le nom du dossier
read -p 'Quel dossier voulez vous archiver ?: ' dossier

# Controle si la variable entrée par l'utilisateur n'est pas vide.
while [ -z $dossier ]
do
  read -p 'Quel dossier voulez vous archiver ?: ' dossier
done

# demande si mon dossier archive qui est dans mon dossier Bureau n'existe pas
# $USER c'est le nom du l'utilisateur connecté
# Cette variable est native

if [ ! -d "/home/$USER/Bureau/archive" ]
then
  # les `` sont à mettre uniquement quand on stocke une commande dans une variable
  mkdir /home/$USER/Bureau/archive
fi

# -e permet d'écrire avec un retour de ligne
# du -sh $dossier/* permet d'afficher la taille de tous les elements dans le dossier
# et je stocke dans info.txt
cmd=`du -sh $dossier/*`
echo -e $cmd > info.txt

date=`date` # commande qui affiche la date
# je commence un variable $log que je concatene
log="======== DEBUT ======\n"
log+="Date: $date\n"
log+="Dossier: $dossier\n"
log+=$cmd
log+="\n======== FIN ======\n"

# je rajoute le contenu de $log dans mon log.txt
echo -e $log >> log.txt


# archive mon fichier $dossier.tgz dans mon /home/$USER/Bureau/archive
# je lui indique de mettre mon $dossier + info.txt
tar -cvf /home/$USER/Bureau/archive/$dossier.tgz $dossier info.txt

# je supprime le fichier info.txt
rm info.txt
