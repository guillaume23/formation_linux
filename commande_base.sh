Commande linux:

Line symbolique: 
# faire un lien symbolique sur le répertoire /var/www nom du raccourci: www
ln -s /var/www/ www

rechercher un fichier: 
locate php.ini # affiche le fichier avec le chemin
find ./ -name "*.php" #affiche le fichier
grep -rn "toto" * # affiche le fichier avec le n° de ligne

# pour faire un alias, il faut modifier le ~/.bash_rc
alias sshA='ssh guillaumebatier@ssh-guillaumebatier.alwaysdata.net'

# voir les paquets installé
sudo dpkg --get-selections

# ajouter les pacquets

sudo dpkg --set-selections < liste_des_paquets.txt

# afficher les processus

ps # affiche les processus en cours

ps -u humanb # affiche les processus de l'utilisateur humanb

ps -aux # affiche tous les process

ps -aux | grep mysql # liste les processus de mysql

# Tuer un processus 

kill PID (numéro du processus) 
killall nom_du_process # touer toutes les instances d'un processus

kill -9 PID #tuer proprement un processus

UTILITAIRE/ 
htop
mc
vim







