#!/bin/bash

# si un dossier n'existe pas
# -d pour directory (dossier)
# le ! signifie "inverse"
# if [ -d 'images' ] = si le dossier existe
# if [ ! -d 'images' ] = si le dossier n'existe pas.

# if [ ! -d "images" ]
# then
#   mkdir images
# fi


html="<html>
    <head></head>
    <body>"

# génération des miniatures
# les `` permettent d'inserer une fonction et non des caractères
# le / peut être retiré à la fin de images/
# le images/*.jpg etc.. permet de "filtrer" les résulat pour ne pas afficher les fichiers non désirable

# cmd_ls=`ls images/*.jpg images/*.gif images/*.jpeg images/*.png `

#recupere uniqument le nom de l'image sans le images/
cmd_ls=`cd images && ls *.jpg *.gif *.jpeg *.png `


for image in $cmd_ls
do
  # liste les images du dossier à partir de la commande dans $cmd_ls
  # je rajoute du contenu dans la variable html (concatenation)
  # on peut aussi filter avec cette manière
  # if [[ images/$image == *.jpg ]]
  # then
  #   echo "$image est en jpg"
  #   # html+='<img src="'$image'" >'
  # fi

    #  Copie l'image et la retaille en 200x200px dans le dossier miniatures/
    # Retaille les image avec les proportion
    # si on met un ! avant le '200x200' les proportions ne sont pas gardés ex: !(200x200)
    convert images/$image -thumbnail '200x200' miniatures/$image

    #affiche l'image du dossier miniatures/ en html
    html+='<img src="miniatures/'$image'" >'
done

html+="</body>
</html>"

echo $html > test.txt
