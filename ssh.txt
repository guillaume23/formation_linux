SSH server

Pour creer un serveur SSH

installer le pacquet openssh-server

sudo apt-get install openssh-server

Puis configurer le fichier de configuration 
/etc/ssh/sshd_config

Pour ne pas autoriser la connexion en root
Changer le 

PermitRootLogin no

Controle des utilisateurs:

#Permet de refuser un / plusieurs utilisateurs
DenyUsers utilisateur1 utilisateur2 

# refuser les groupes
DenyGroups group1 group2 

# Autoriser un ou plusieurs utilisateurs
AllowUsers user1 user2 

#autoriser uniquement un ou plusieurs groupes
AllowGroups group1 group2 

















