#!/bin/bash
# demander à l'utilsateur le dossier à créér puis le stock
# dans la variable $dossier
# -p afficher un message en console
read -p "Dans quel dossier voulez vous créér l'application:" dossier

mkdir $dossier
touch $dossier/index.html
mkdir $dossier/css
mkdir $dossier/js
mkdir $dossier/img

touch $dossier/css/styles.css
touch $dossier/js/init.js

touch $dossier/info.txt
nom_du_dossier=̀`pwd`
liste_fichiers=`ls`
message="Voici le dossier actuel ($nom_du_dossier/$dossier):"
message+='\n'
message+=$liste_fichiers

# echo -e prends en compte les sauts de ligne
echo -e $message > $dossier/info.txt
