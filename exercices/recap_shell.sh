#!/bin/bash

# Ceci est un commentaire - le code n'est pas executé
# Creation d'une variable

# $USER = variable du systeme qui contient le nom de l'utilisateur connecté
# message="Bienvenue $USER !"
# message+=' '
# message+=$USER
# message+=' !'


#message='BienvuUN '$USER' !'
message="BienvuUN $USER !"
# Meme resultat

echo "${message^}" # echo Bienvenue avec un B MAJ
echo "${message^^}" # echo tout en MAJ
echo "${message,}" # echo la premiere lettre en minuscule
echo "${message,,}" # echo tout en minuscule

commande_pwd=`ls /etc && ls /home`

echo "Voici le resultat de la commande $commande_pwd"
