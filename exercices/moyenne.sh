#!/bin/bash

read -p "Quelle est votre moyenne:" moyenne

while ! [[ "$moyenne" =~ ^[0-9]+$ ]] || [ -z $moyenne  ]
do
  read -p "Quelle est votre moyenne:" moyenne
done

if [ $moyenne -ge 16 ]
then
  message="Excellent !"

elif [ $moyenne -ge 14 ]
then
  message="bien"
elif [ $moyenne -ge 12 ]
then
    message="assez bien"
elif [ $moyenne -ge 10 ]
then
    message="passable"
else
    message="C'est mauvais"
fi

echo -e $message
