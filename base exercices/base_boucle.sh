#!/bin/bash

#boucle for
let "i=0"
# for i in {1..100}

# la boucle passe 3 fois car j'ai mis 3 paramètres 'john' 'mary' 'bob'
# a chaque passe la variable $prenom vaut la valeur passée
# exemple 1er passage $prenom vaut 'john'
# 2eme passage $prenom vaut 'mary'
# 3eme passage $prenom vaut 'bob'

for prenom in 'john' 'mary' 'bob'
do
  let "i=i+1" #i s'additionne de 1 à chaque passage
  echo "$prenom listé en position $i"
done
echo "Vous avez listé $i personnes"
