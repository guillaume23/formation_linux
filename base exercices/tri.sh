#!/bin/bash

# lister un dossier via une boucle
# et compter le nombre d'éléments à l'intérieur

# INDICE: faire la boucle for avec une commande ls

# allez plus loin : Compter le nombre de dossiers et de fichiers
# demander à l'utilisateur quel dossier lister

read -p "Quel dossier voulez vous lister ?:" dossier
# je mets numériquement 0 à i
let "nb_fichiers=0"
let "nb_dossiers=0"
let "nb_autre=0"
# je fais une variable contenant la commande ls /home
cmd_ls=`ls -R $dossier`

for element in $cmd_ls
do
  if [[ -d $dossier/$element ]]
  then
    let "nb_dossiers=nb_dossiers+1"
  elif [[ -f $dossier/$element ]]
  then
      let "nb_fichiers=nb_fichiers+1"
    else
        let "nb_autre=nb_autre+1"
  fi

done
# echo -e car retour de ligne:
echo -e "Il y a: \n $nb_dossiers dossier(s)\n $nb_fichiers fichier(s)\n $nb_autre fichiers inconnus"



# # detecter si un element est un dossier:
# if [[ -d $element ]]
# then
#   #ceci est un dossier
# fi
#
# # detecter si un element est un fichierœ:
# if [[ -f $element ]]
# then
#   #ceci est un fichier
# fi
