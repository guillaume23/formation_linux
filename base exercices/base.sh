#!/bin/bash

# declarer une variable

nom="Batier"
prenom="Guillaume"

# Differene entre double quote et simple quote
message='Bienvenue '$nom' '$prenom
message="Bienvenue $nom $prenom"
echo $message

# concatenation avec la mention +=
message='Bienvenue'
message+=' '
message+=$nom
message+=' '
message+=$prenom
echo $message
