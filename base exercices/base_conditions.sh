#!/bin/bash

# 1 - Demande le nom de l'utilisateur
# 2 - Une fois le nom rentré, afficher "Salut" + le nom de l'utilisateur si l'utilisateur se nomme john sinon mettre bonjour + le nom de l'utilisateur

# si il s'agit de bruno, afficher un message "Hey salut bruno ça va ? "
# Si il s'agit du patron: "Bonjour patron, voulez vous un café ?"

read -p "Quel est ton nom ?" nom

# = signifie -> assigne une valeur à une variable
# == permet de tester si la valeur est égale à ...

if [ $nom == "john" ]
then
  echo "Salut $nom"
elif [ $nom == "bruno" ]
then
  echo "Hey salut $nom ça va ?"
elif [ $nom == "patron" ]
then
  echo "Bonjour $nom, voulez vous un café ?"
else
  echo "Bonjour $nom"
fi
