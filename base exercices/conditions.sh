#!/bin/bash
read -p "Quelle table de multiplication voulez vous voir ?" table

# à chaque passage la variable multiple vaut +1
# ex: premier passage $multiple = 1
# 2eme passage $multiple = 2
# 3eme passage $multiple = 3 etc...

for multiple in {1..10}
do
  # let force un calcul mathématique
  # ce n'est pas la peine de mettre le $ car il comprend que c'est une variable
  let "calcul=table*multiple"
  #J'écris ensuite le cacul ex: 6 x 3 = 18
  echo "$table x $multiple = $calcul"
done
